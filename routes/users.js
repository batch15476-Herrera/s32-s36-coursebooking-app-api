//[SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controller/users');
const User = require('../models/User');
const auth = require('../auth');

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] Routes- POST
route.post('/register', (req, res) => {
    console.log(req.body);
    let userData = req.body;
    controller.register(userData).then(outcome=> {
        res.send(outcome);
    });

});

//[SECTION] Route for User Authentication(Login)
route.post('/login', (req, res) => {
	controller.loginUser(req.body).then(result => res.send(result));
});

//[SECTION] Routes GET the user's details
route.get('/details', auth.verify, (req, res) => {
	controller.getProfile(req.user.id).then(result => res.send(result));
});


//Enroll our registered Users
//only the verified user can enroll in a course
route.post('/enroll', auth.verify, controller.enroll);


//[SECTION] Expose Route System
module.exports = route; 
