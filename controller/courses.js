const Course = require('../models/Course');


//Create a new course 
/*
steps:
    1. Create a new course object using mongoose model and the information from the request body
    2. Save the new Course to the data base
*/

module.exports.addCourse = (reqBody) => {
        //create a variable "newCourse" and instantiate name, description and price.
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        });


    //save the created object to our database
    return newCourse.save().then((course,error) => {
        //Course creation is successful or not
        if(error) {
            return false;
        } else {
            return true;
        }
    }).catch(error => error);
};


//Retrive all courses

//1. retrieve all the courses from the database

module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    }).catch (error => error);
};

//retrieve all active course
module.exports.getAllActive = () => {
    return Course.find({isActive:true}).then(result => {
        return result;
    }).catch (error => error);
};

//retrieving a specific course
//1. retrieve the cousrse that matches the course ID provided from the URL

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams).then(result => {
        return result
    }).catch (error => error);
};


//Update a course
/*
STEPS
*/

module.exports.updateCourse = (courseId, data) => {
	//specify the fields/properties of the document to be updated
	let updatedCourse = {
		name: data.name,
		description: data.description,
		price: data.price
	}

	//findByIdAndUpdate(document Id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error);
};



//Archiving a course
//update the status of "isActive" into "false" which no longer be displayed in the client whenever all active course are retrieved
//Archiving a course
//1. update the status of "isActive" into "false" which will no longer be displayed in the client whenever all active courses are retrieved.
module.exports.archiveCourse = (courseId) => {
	let updateActiveField = {
		isActive: false
	};

	return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error);
};
